# String list manipulator

A python library developed to substitute entities in a longer text.
Its most important function are used to:
  
- Modify elements in a string list to be 'abc_def' from 'Abc def'
 
- Substitute the match in the message with the first not empty element of another list (each letter alternated with a '-')

- Delete nth element in all lists if the nth elements of a particular list is true when evaluated with a custom condition

### Installation
The best way to install is to use pip:
```sh
$ pip install strlistsmanipulator
```
Other information on how to install: https://packaging.python.org/tutorials/installing-packages/#installing-from-pypi
### License
MIT